const CART_LS = "cart";

function addCartToLS(newProduct) {
  const carts = getCartLS();
  //1. Kiểm tra sp này đã tồn tại trong cart chưa
  //const isExists = carts.some(item => item.id == newProduct.id);
  // isExists = true || false
  // const index = carts.findIndex(item => item.id == newProduct.id);
  // const product = carts.find(item => item.id == newProduct.id);
  // let isExist = false
  // for(const item of carts) {
  //     if(item.id == newProduct.id) {
  //         isExist = true
  //         break
  //     }
  // };
  const index = carts.findIndex((item) => item.id == newProduct.id);

  //2. Nếu tồn tại rồi thì update qty (công lên 1)
  if (index >= 0) {
    const newQty = carts[index].qty + 1;
    if (newQty > 10) {
      return;
    }
    carts[index].qty = newQty;
    carts[index].price = newQty * carts[index].unit_price;
  } else {
    carts.push(newProduct);
    // Nếu chưa insert như bình thường
  }
  localStorage.setItem(CART_LS, JSON.stringify(carts));
}

// [
//     { id: 1, name: 'dfgdf'},
//     {id: 2, qty: 3, name: ''},
//      {id: 3, qty: 1, name: ''}
// ]
function getCartLS() {
  const data = localStorage.getItem(CART_LS);
  return data ? JSON.parse(data) : [];
}

function updateCartQty(id, qty) {
  const carts = getCartLS();
  const index = carts.findIndex((item) => item.id == id);
  if (index < 0) {
    return;
  }
  const newQty = carts[index].qty + qty;
  if (newQty <= 0) {
    removeItem(id);
    return;
  }
  if (newQty > 10) {
    return showLimitQty(), hideSideNav();
  }
  carts[index].qty = newQty;
  carts[index].price = newQty * carts[index].unit_price;
  localStorage.setItem(CART_LS, JSON.stringify(carts));
}

function removeItemLS(id) {
  const carts = getCartLS();
  const index = carts.findIndex((item) => item.id == id);
  if (index < 0) {
    return;
  }

  carts.splice(index, 1);
  localStorage.setItem(CART_LS, JSON.stringify(carts));
}

function clearCartLS() {
  // trả về array rỗng
  localStorage.setItem(CART_LS, JSON.stringify([]));
}
