function addToCart(product) {
  addCartToLS({
    id: product.id,
    name: product.name,
    // parseInt ~ chỉ lấy số nguyên
    // parseFloat ~ lấy số thập phân
    price: parseFloat(product.price),
    unit_price: parseFloat(product.price),
    img: product.img,
    qty: 1,
  });
  reLoadCart();
}

function reLoadCart() {
  // 1. get từ LS
  const allCart = getCartLS();
  console.log("allCart: ", allCart);
  // 2. sum qty all product
  let totalQty = 0;
  allCart.forEach((cart) => {
    totalQty += cart.qty;
  });
  // => tính số lượng SP
  // 3. render qty ra UI
  document.getElementById("total-qty").innerHTML = totalQty;
  totalPrice();
  renderCart();
}

function totalPrice() {
  const allCart = getCartLS();
  // 2. sum price all product
  let result = allCart.reduce((total, item) => {
    return total + item.price
  }, 0)
  document.getElementById("total").innerHTML = result;
  // let total = 0;
  // allCart.forEach((cart) => {
  //   total += cart.price;
  // });
  // document.getElementById("total").innerHTML = total;
}

function renderCart() {
  const allCart = getCartLS();
  // 4. render sp trong cart ra UI
  let contentHTML = "";
  allCart.forEach(function (item) {
    contentHTML += `<div class="cart-item">
        <div class="cart-img">
            <img src="${item.img}" alt="">
        </div>
        <strong class="name">${item.name}</strong>
        <span class="qty-change">
            <div>
                <button class="btn-qty" onclick="updateQty(${item.id}, -1)"><i
                        class="fas fa-chevron-left"></i></button>
                <p class="qty">${item.qty}</p>
                <button class="btn-qty" onclick="updateQty(${item.id}, 1)"><i
                        class="fas fa-chevron-right"></i></button>
            </div>
        </span>
        <p class="price">${item.price}</p>
        <button onclick="removeItem(${item.id})"><i class="fas fa-trash"></i></button>
    </div>`;
  });
  document.getElementById("cart-items").innerHTML = contentHTML;
}

function rederPurchase() {
  const allCart = getCartLS();

  const nameHtml = allCart.map(item => `<span>${item.qty} x ${item.name}</span>`)
  const priceHtml = allCart.map(item => `<span>${item.price}</span>`)

  const result = allCart.reduce((total, item) => {
    return total + item.price
  }, 0)
  document.getElementById("pay").innerHTML = result;


  document.getElementById("item-names").innerHTML = nameHtml.join('');
  document.getElementById("items-price").innerHTML = priceHtml.join('+');
}

function rederOrder() {
  const allCart = getCartLS();
  const result = allCart.reduce((total, item) => {
    return total + item.price
  }, 0)
  document.getElementById("payment-total").innerHTML = result;
}

function updateQty(id, qty) {
  updateCartQty(id, qty);
  reLoadCart();
}

function removeItem(id) {
  removeItemLS(id);
  reLoadCart();
}

function clearCart() {
  clearCartLS();
  reLoadCart();
}

function showLimitQty() {
  document.getElementById("cover purchase-cover").style.display = "block";
  document.getElementById("stock-limit").style.display = "flex";
}

function hideLimitQty() {
  document.getElementById("cover purchase-cover").style.display = "none";
  document.getElementById("stock-limit").style.display = "none";
}

function showsSideNav() {
  document.getElementById("cover").style.display = "block";
  document.getElementById("side-nav").style.right = "0px";
}

function hideSideNav() {
  document.getElementById("cover").style.display = "none";
  document.getElementById("side-nav").style.right = "-100%";
}

// phân biệt obj js và obj mình tạo ra

// chỉnh tăng qty nếu add trùng sp
